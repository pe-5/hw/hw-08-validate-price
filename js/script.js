const formPrice = document.querySelector('#formPrice')
const price = document.querySelector('#price')
const btn = document.createElement('button')
const span = document.createElement('span')

btn.insertAdjacentHTML('afterbegin',`<i class='far fa-times-circle'></i>`);

formPrice.addEventListener("focusin", () => {
  formPrice.className = 'focus'
  span.remove()
});

formPrice.addEventListener("focusout", () => {
  formPrice.classList.remove('focus')

  if(price.value === null || Number.isNaN(+price.value) || Number.isInteger(price.value) || price.value < 0 || (price.value.trim() === "" && price.value.length > 0)){
    formPrice.after(span)
    formPrice.append(btn)
    formPrice.className = 'error'
    span.className = 'incorrect'
    span.innerText = `Please enter correct price!`
  }
  
  else if(price.value != ""){
    formPrice.before(span)
    formPrice.append(btn)
    formPrice.className = 'correctValue'
    span.className = 'correct'
    span.innerText = `Current price: ${price.value}$`
  }
})

removeEl = () =>{
  price.value = '';
  span.remove();
  btn.remove()
}

btn.addEventListener("click", removeEl);